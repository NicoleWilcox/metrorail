# README #

This is the repo for my personal project. It is a site redesign for Metrorail (South Africa). 

* version 0.1.0
* author: Nicolew | Miss Code 
* Bootstrap 4

### Who is Metrorail? ###

* Metrorail is an operator of commuter rail services in the major urban areas of South Africa.
* See more: https://en.wikipedia.org/wiki/Metrorail_(South_Africa)

### Reason for project: ###

* The current Metrorail website is a complete fail (http://www.metrorail.co.za/)
* This redesign is most needed. 